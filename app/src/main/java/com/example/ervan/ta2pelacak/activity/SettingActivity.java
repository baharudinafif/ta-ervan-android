package com.example.ervan.ta2pelacak.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ervan.ta2pelacak.R;
import com.example.ervan.ta2pelacak.service.LocationRetrieval;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.Header;

public class SettingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        final String Mypref="mypref";
        final SharedPreferences sharedPref = this.getSharedPreferences(Mypref,Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPref.edit();

        Button simpan=(Button) findViewById(R.id.simpan_setting);

        final EditText Inputpin=(EditText) findViewById(R.id.editPIN);
        final EditText Inputname=(EditText) findViewById(R.id.editName);

        Context context = getApplicationContext();
        CharSequence text = "Data Berhasil Disimpan";
        int duration = Toast.LENGTH_SHORT;

        final Toast toast = Toast.makeText(context, text, duration);


        TextView pin=(TextView) findViewById(R.id.setting_pin);
        String defaultValue = getResources().getString(R.string.pin);
        String PIN = sharedPref.getString(getString(R.string.pin), defaultValue);
        Log.i(defaultValue, PIN);
        pin.setText(PIN);

        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String key;
                final long timestamp        = System.currentTimeMillis() / 1000;
                final String inputpinvalue  = Inputpin.getText().toString();
                final String inputnamavalue = Inputname.getText().toString();

                Log.i("pin", inputpinvalue.length() + "");
                Log.i("nama", inputnamavalue.length() + "");

                if (inputpinvalue.length() == 0 || inputnamavalue.length() == 0) {
                    toast.setText("Terdapat isian yang kosong");
                } else {
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.get("http://128.199.190.244/index.php/user/get/"+inputpinvalue, new JsonHttpResponseHandler(){
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            Log.i("response", String.valueOf(response));
                            try {
                                Map<String, ?> keys = sharedPref.getAll();
                                Pattern p           = Pattern.compile("Device\\d+-PIN");
                                String prevKey      = "";
                                for (Map.Entry<String, ?> entry : keys.entrySet()) {
                                    Log.i("map values", entry.getKey() + " : " + entry.getValue().toString());
                                    String temp     = entry.getKey();
                                    Matcher m       = p.matcher(temp);
                                    Boolean samePin = entry.getValue().toString().trim().toUpperCase().equals(inputpinvalue.trim().toUpperCase());
                                    Log.i("Cond", m.matches() + " "+ samePin);
                                    if (m.matches() && samePin) {
                                        prevKey = entry.getKey().replaceAll("-PIN", "");
                                    }
                                }

                                Log.i("status", response.getString("status"));
                                Log.i("prevKey", "|||" + prevKey);
                                if(response.getString("status").equals("200")){
                                    toast.setText("data berhasil disimpan");
                                    JSONArray array = response.getJSONArray("data");
                                    JSONObject data = (JSONObject) array.get(0);
                                    if (prevKey.length() > 0) {
                                        editor.putString(prevKey + "_lat",data.getString("latitude"));
                                        editor.putString(prevKey + "_long",data.getString("longitude"));
                                        editor.putString(prevKey + "-PIN",inputpinvalue);
                                        editor.putString(prevKey + "-NAMA",inputnamavalue);
                                    } else {
                                        editor.putString("Device" +  timestamp +"_lat",data.getString("latitude"));
                                        editor.putString("Device" +  timestamp +"_long",data.getString("longitude"));
                                        editor.putString("Device" +  timestamp +"-PIN",inputpinvalue);
                                        editor.putString("Device" +  timestamp +"-NAMA",inputnamavalue);
                                    }
                                    editor.apply();

                                    finish();
                                } else {
                                    toast.setText("data yang dimasukan salah");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            toast.show();
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            super.onFailure(statusCode, headers, responseString, throwable);
                            Log.i(statusCode+"",responseString);
                        }
                    });
                }
            }
        });
    }
}