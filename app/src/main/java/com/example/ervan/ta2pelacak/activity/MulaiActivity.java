package com.example.ervan.ta2pelacak.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.provider.UserDictionary;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.ervan.ta2pelacak.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.Header;

public class MulaiActivity extends AppCompatActivity {
    Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mulai);

        final Button lacakBtn   = (Button) findViewById(R.id.lacakBtn);
        final Context Ctx       = this.getApplicationContext();
        toast                   = Toast.makeText(Ctx, "Data Kosong", Toast.LENGTH_SHORT);
    }

    @Override
    protected void onResume() {
        super.onResume();

        final ArrayAdapter<String> adapter;
        final Button lacakBtn       = (Button) findViewById(R.id.lacakBtn);
        final Spinner spinner       = (Spinner) findViewById(R.id.spinner);
        final List<String> list     = new ArrayList<String>();
        final List<String> content  = new ArrayList<String>();

        final String Mypref="mypref";
        final SharedPreferences sharedPref      = this.getSharedPreferences(Mypref, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor   = sharedPref.edit();

        Map<String, ?> keys = sharedPref.getAll();

        Pattern p           = Pattern.compile("Device\\d+-NAMA");

        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            Log.i("map values", entry.getKey() + " : " + entry.getValue().toString());
            String temp = entry.getKey();
            Matcher m   = p.matcher(temp);
            if (m.matches()) {
                list.add(entry.getValue().toString());
            }
        }

        adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                content.clear();

                String selectedItem = list.get(i);
                Boolean found       = false;
                Log.i("Selected Item", selectedItem);

                Map<String, ?> map = sharedPref.getAll();
                for (Map.Entry<String, ?> entry : map.entrySet()) {
                    if (entry.getValue().toString().trim().toUpperCase().equals(selectedItem.trim().toUpperCase()) && !found){
                        Log.i("selected", selectedItem + " ||| " + entry.getValue().toString());
                        found = true;
                        String foundKey = entry.getKey().replace("-NAMA", "") + "-PIN";
                        content.add(selectedItem);
                        content.add(sharedPref.getString(foundKey, ""));

                    }
                }
                Log.d("content", content.toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                content.clear();
            }
        });

        lacakBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (content.size() == 0) {
                    toast.show();
                } else {
                    final String inputpinvalue = content.get(1);
                    Log.i("TEST", inputpinvalue);
                    Log.i("Asking for loc", inputpinvalue);
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.get("http://128.199.190.244/index.php/user/get/" + inputpinvalue, new JsonHttpResponseHandler(){
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            Log.i("response", String.valueOf(response));
                            try {
                                Log.i("status", response.getString("status"));
                                if(response.getString("status").equals("200")){
                                    JSONArray data = response.getJSONArray("data");

                                    Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                                    intent.putExtra("nama", content.get(0));
                                    intent.putExtra("array", data.toString());
                                    startActivity(intent);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            super.onFailure(statusCode, headers, responseString, throwable);
                            Log.i(statusCode + "",responseString);
                        }
                    });
                }
            }
        });
    }
}

