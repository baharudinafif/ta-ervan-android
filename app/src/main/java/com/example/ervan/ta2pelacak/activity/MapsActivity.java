package com.example.ervan.ta2pelacak.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.FloatRange;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.ervan.ta2pelacak.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String nama;
    private JSONArray array;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        nama    = this.getIntent().getExtras().getString("nama");
        try {
            array   = new JSONArray(this.getIntent().getExtras().getString("array"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng location = null;


        for(int i = 0; i < array.length(); ++i) {
            try {
                JSONObject temp = (JSONObject) array.get(i);
                Float latitude  = Float.parseFloat(temp.getString("latitude"));
                Float longitude = Float.parseFloat(temp.getString("longitude"));
                Integer hour    = Integer.parseInt(temp.getString("hour"));

                location        = new LatLng(latitude, longitude);
                String text     = nama + " berada disini jam" + hour.toString() + ".00";
                Log.i("data-" + i, text);
                Log.i("data-" + i, latitude.toString() + " " + longitude.toString());
                mMap.addMarker(new MarkerOptions().position(location).title(text));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(location)
                .zoom(15)                   // Sets the zoom
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
}
